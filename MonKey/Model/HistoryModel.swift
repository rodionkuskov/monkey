import Foundation
import Apollo

class HistoryModel {
    
    var categoryNameArray: [String]
    var amountArray: [Double]
    var titleArray: [String]
    var accountName: [String]
    var accountId: [GraphQLID]
    
    init(categoryName: [String], amount: [Double], title: [String], accountName: [String], accountId: [GraphQLID]) {
        self.categoryNameArray = categoryName
        self.amountArray = amount
        self.titleArray = title
        self.accountName = accountName
        self.accountId = accountId
    }
}
