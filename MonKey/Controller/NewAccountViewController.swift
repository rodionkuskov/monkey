//
//  NewAccountViewController.swift
//  MonKey
//
//  Created by Rodion Kuskov on 11/12/18.
//  Copyright © 2018 Rodion Kuskov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Apollo

class NewAccountViewController: UIViewController {
    
    let notification = UINotificationFeedbackGenerator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .cyan
        addSubview()
        addLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.title = "Счета"
    }
    
    lazy var accountName: UITextField = {
        let field = UITextField()
        field.backgroundColor = .white
        field.textAlignment = .center
        field.placeholder = "Добавьте название"
        field.setBottomBorder()
        return field
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView(style: .gray)
        ai.frame = view.bounds
        return ai
    }()
    
    lazy var accountMoney: UITextField = {
        let field = UITextField()
        field.backgroundColor = .white
        field.textAlignment = .center
        field.placeholder = "Количество денег на счету"
        field.setBottomBorder()
        field.keyboardType = .decimalPad
        return field
    }()
 
    lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Сохранить", for: .normal)
        button.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 17
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        button.addTarget(self, action: #selector(saveAccount), for: .touchUpInside)
        button.tag = 0
        return button
    }()
    
    @objc func saveAccount() {
        activityIndicator.startAnimating()
        guard let accountName = accountName.text else {
            showAlert(title: "Что-то пошло не так...", message: "Проверьте, что вы заполнили все поля")
            return
        }
        
        let accountMoney = self.accountMoney.text
        let amount = (accountMoney! as NSString).doubleValue

        LoginManager.shared.apollo.perform(mutation: CreateAccountMutation(name: accountName, balance: amount)) { [unowned self] (result, error) in
            if let error = error {
                self.activityIndicator.stopAnimating()
                self.showAlert(title: "Что-то пошло не так...", message: "\(error)")
            }
            self.creatingSuccess()
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Понял", style: .default, handler: { (_) in
            self.activityIndicator.stopAnimating()
        }))
        
        self.present(alert, animated: true)
    }

    
    func creatingSuccess() {
        self.activityIndicator.stopAnimating()
        self.notification.notificationOccurred(.success)
        RootVCSwitcher.shared.presentTab()
    }
    
    func addSubview() {
        view.addSubview(accountName)
        view.addSubview(accountMoney)
        view.addSubview(saveButton)
        view.addSubview(activityIndicator)
    }
    
    func addLayout() {
        
        accountName.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(-15)
            make.height.equalTo(30)
            make.center.equalToSuperview()
        }
        
        accountMoney.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(-15)
            make.height.equalTo(30)
            make.top.equalTo(accountName.snp.bottom).offset(10)
        }
        
        saveButton.snp.makeConstraints { (make) in
            make.width.equalTo(115)
            make.height.equalTo(35)
            make.top.equalTo(accountMoney.snp.bottom).offset(10)
        }
        
        
    }
}
