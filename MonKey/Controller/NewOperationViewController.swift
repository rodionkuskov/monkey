//
//  NewOperationViewController.swift
//  MonKey
//
//  Created by Rodion Kuskov on 10/28/18.
//  Copyright © 2018 Rodion Kuskov. All rights reserved.
//

import UIKit
import Foundation
import SnapKit
import Apollo
import AVFoundation

class NewOperationViewController: UIViewController {
    
    static var accountId: GraphQLID?
    static var categoryId: GraphQLID?
    static var toAccountId: GraphQLID?
    
    var currentButtonTag: Int?
    
    let notification = UINotificationFeedbackGenerator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Добавить сумму"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(saveOperation))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        subview()
        layout()
        showDatePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    let datePicker = UIDatePicker()
    
    lazy var incomeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Доход", for: .normal)
        button.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        button.backgroundColor = .white
        button.layer.cornerRadius = 17
        button.layer.borderWidth = 1
        button.tag = 1
        button.addTarget(self, action: #selector(changeButtonColor), for: .touchUpInside)
        return button
    }()
    
    lazy var transactionButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Перевод", for: .normal)
        button.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 17
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        button.addTarget(self, action: #selector(changeButtonColor), for: .touchUpInside)
        button.tag = 0
        return button
    }()
    
    lazy var outcomeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Расход", for: .normal)
        button.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 17
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        button.addTarget(self, action: #selector(changeButtonColor), for: .touchUpInside)
        button.tag = 2
        return button
    }()
    
    lazy var txtDatePicker: UITextField = {
        let datePicker = UITextField()
        datePicker.backgroundColor = .white
        datePicker.textAlignment = .right
        datePicker.placeholder = "Выберите дату"
        datePicker.setBottomBorder()
        return datePicker
    }()
    
    lazy var selectDateLabel: UILabel = {
        let label = UILabel()
        label.text = "📅 Дата"
        label.textColor = .lightGray
        return label
    }()
    
    lazy var noteLabel: UILabel = {
        let label = UILabel()
        label.text = "🖋 Примечание"
        label.textColor = .lightGray
        return label
    }()
    
    lazy var amountLabel: UILabel = {
        let label = UILabel()
        label.text = "💸 Сумма"
        label.textColor = .lightGray
        return label
    }()
    
    lazy var payeeLabel: UILabel = {
        let label = UILabel()
        label.text = "🦹🏻‍♂️ Назначение"
        label.textColor = .lightGray
        return label
    }()
    
    lazy var walletLabel: UILabel = {
        let label = UILabel()
        label.text = "💳 Счет"
        label.textColor = .lightGray
        return label
    }()
    
    lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.text = " Категория"
        label.textColor = .lightGray
        return label
    }()
    
    lazy var transferLabel: UILabel = {
        let label = UILabel()
        label.text = " На кошелек"
        label.textColor = .lightGray
        label.isHidden = true
        return label
    }()
    
    lazy var chooseWalletButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Выбрать кошелек  ", for: .normal)
        button.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        button.frame = CGRect(x: 0, y: 0, width: 0, height: 35)
        button.layer.cornerRadius = 17
        button.addTarget(self, action: #selector(openWalletView), for: .touchUpInside)
        return button
    }()
    
    lazy var transferToWallet: UIButton = {
        let button = UIButton()
        button.setTitle("  На кошелек  ", for: .normal)
        button.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        button.frame = CGRect(x: 0, y: 0, width: 0, height: 35)
        button.layer.cornerRadius = 17
        button.addTarget(self, action: #selector(openTransferWalletView), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    lazy var chooseCategoryButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Выбрать категорию  ", for: .normal)
        button.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        button.frame = CGRect(x: 0, y: 0, width: 0, height: 35)
        button.layer.cornerRadius = 17
        button.addTarget(self, action: #selector(openCategoryView), for: .touchUpInside)
        return button
    }()
    
    lazy var amountTextField: UITextField = {
        let field = UITextField()
        field.backgroundColor = .white
        field.textAlignment = .right
        field.placeholder = "Добавьте сумму"
        field.keyboardType = .decimalPad
        field.resignFirstResponder()
        field.setBottomBorder()
        return field
    }()
    
    lazy var noteTextField: UITextField = {
        let field = UITextField()
        field.backgroundColor = .white
        field.textAlignment = .right
        field.placeholder = "Добавьте примечание"
        field.setBottomBorder()
        return field
    }()
    
    lazy var payeeTextField: UITextField = {
        let field = UITextField()
        field.backgroundColor = .white
        field.textAlignment = .right
        field.placeholder = "Добавьте назначение"
        field.setBottomBorder()
        return field
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView(style: .gray)
        ai.frame = view.bounds
        return ai
    }()
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(datePickerFunc));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDatePicker.inputAccessoryView = toolbar
        txtDatePicker.inputView = datePicker
    }
    
    @objc func openWalletView() {
        let vc = WalletViewController()
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func openTransferWalletView() {
        let vc = TransferViewController()
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func openCategoryView() {
        let vc = CategoryViewController()
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func saveOperation() {
        activityIndicator.startAnimating()
        
        let amountString = amountTextField.text
        let amount = (amountString! as NSString).doubleValue
        let payee = payeeTextField.text ?? ""
        let toAccountId = NewOperationViewController.toAccountId ?? ""
        
        guard
            let date = txtDatePicker.text,
            let comment = noteTextField.text else {
                showAlert(title: "Что-то пошло не так...", message: "Скорее всего вы не выбрали дату/комментарий")
                return
        }
        
        guard
            let accountId = NewOperationViewController.accountId else {
                showAlert(title: "Что-то пошло не так...", message: "Скорее всего вы не выбрали счет")
                return
        }
        
        let categoryId = NewOperationViewController.categoryId

        // REFACTOR THIS SHIT
        if currentButtonTag == 1 {
            LoginManager.shared.apollo.perform(mutation: CreateIncomeMutation(amount: amount, accountId: accountId, date: date, comment: comment, place: "Детдом", categoryId: categoryId)) { [unowned self] (result, error) in
                if let error = error {
                    self.activityIndicator.stopAnimating()
                    self.showAlert(title: "Что-то пошло не так...", message: "\(error)")
                }
                
                self.creatingSuccess()
            }
            
        } else if currentButtonTag == 2 {
            LoginManager.shared.apollo.perform(mutation: CreateExpenseMutation(amount: amount, accountId: accountId, date: date, comment: comment, payee: payee, categoryId: categoryId)) { [unowned self] (result, error) in
                if let error = error {
                    self.activityIndicator.stopAnimating()
                    self.showAlert(title: "Что-то пошло не так...", message: "\(error)")
                }
                
                self.creatingSuccess()
            }
            
        } else if currentButtonTag == 0 {
            LoginManager.shared.apollo.perform(mutation: CreateTransferMutation(amount: amount, accountId: accountId, date: date, comment: comment, toAccountId: toAccountId)) { [unowned self] (result, error) in
                if let error = error {
                    self.activityIndicator.stopAnimating()
                    self.showAlert(title: "Что-то пошло не так...", message: "\(error)")
                }
                self.creatingSuccess()
            }
            
        } else {
            showAlert(title: "Что-то пошло не так...", message: "Скорее всего вы не выбрали категорию")
        }
    }
    
    func creatingSuccess() {
        self.activityIndicator.stopAnimating()
        self.notification.notificationOccurred(.success)
        RootVCSwitcher.shared.presentTab()
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Понял", style: .default, handler: { (_) in
            self.activityIndicator.stopAnimating()
        }))
        
        self.present(alert, animated: true)
    }

    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        amountTextField.resignFirstResponder()
    }
    
    @objc func changeButtonColor(button: UIButton) {
        if button.tag  == 0 {
            transactionButton.backgroundColor = UIColor.OperationColor.categoryColor
            transactionButton.setTitleColor(.white, for: .normal)
            
            incomeButton.backgroundColor = .white
            incomeButton.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
            incomeButton.layer.borderWidth = 1
            incomeButton.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
            
            outcomeButton.backgroundColor = .white
            outcomeButton.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
            outcomeButton.layer.borderWidth = 1
            outcomeButton.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
            
            transferToWallet.isHidden = false
            transferLabel.isHidden = false
            chooseCategoryButton.isHidden = true
            categoryLabel.isHidden = true
            self.currentButtonTag = 0
            
        } else if button.tag == 1 {
            incomeButton.backgroundColor = UIColor.OperationColor.categoryColor
            incomeButton.setTitleColor(.white, for: .normal)
            
            transactionButton.backgroundColor = .white
            transactionButton.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
            transactionButton.layer.borderWidth = 1
            transactionButton.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
            
            outcomeButton.backgroundColor = .white
            outcomeButton.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
            outcomeButton.layer.borderWidth = 1
            outcomeButton.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
            
            self.currentButtonTag = 1
            transferToWallet.isHidden = true
            transferLabel.isHidden = true
            chooseCategoryButton.isHidden = false
            categoryLabel.isHidden = false
            
        } else if button.tag == 2 {
            outcomeButton.backgroundColor = UIColor.OperationColor.categoryColor
            outcomeButton.setTitleColor(.white, for: .normal)
            
            incomeButton.backgroundColor = .white
            incomeButton.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
            incomeButton.layer.borderWidth = 1
            incomeButton.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
            
            transactionButton.backgroundColor = .white
            transactionButton.setTitleColor(UIColor.OperationColor.categoryColor, for: .normal)
            transactionButton.layer.borderWidth = 1
            transactionButton.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
            
            self.currentButtonTag = 2
            transferToWallet.isHidden = true
            transferLabel.isHidden = true
            chooseCategoryButton.isHidden = false
            categoryLabel.isHidden = false
        }
    }
    
    
    
    @objc func datePickerFunc(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        txtDatePicker.text = formatter.string(from: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

    func layout() {
        incomeButton.snp.makeConstraints { (make) in
            make.width.equalTo(115)
            make.height.equalTo(35)
            make.top.equalToSuperview().offset(80)
            make.centerX.equalToSuperview()
        }
        
        outcomeButton.snp.makeConstraints { (make) in
            make.width.equalTo(115)
            make.height.equalTo(35)
            make.top.equalToSuperview().offset(80)
            make.left.equalTo(incomeButton.snp.right).offset(10)
        }
        
        transactionButton.snp.makeConstraints { (make) in
            make.width.equalTo(115)
            make.height.equalTo(35)
            make.top.equalToSuperview().offset(80)
            make.right.equalTo(incomeButton.snp.left).offset(-10)
        }
        
        selectDateLabel.snp.makeConstraints { (make) in
            make.width.equalTo(120)
            make.height.equalTo(35)
            make.top.equalTo(transactionButton.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(15)
        }
        
        txtDatePicker.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(-35)
            make.height.equalTo(35)
            make.top.equalTo(transactionButton.snp.bottom).offset(35)
            make.right.equalToSuperview().offset(-20)
        }
        
        noteLabel.snp.makeConstraints { (make) in
            make.width.equalTo(130)
            make.height.equalTo(35)
            make.top.equalTo(txtDatePicker.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(15)
        }
        
        noteTextField.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(-35)
            make.height.equalTo(35)
            make.top.equalTo(selectDateLabel.snp.bottom).offset(35)
            make.right.equalToSuperview().offset(-20)
        }
        
        amountLabel.snp.makeConstraints { (make) in
            make.width.equalTo(130)
            make.height.equalTo(35)
            make.top.equalTo(noteTextField.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(15)
        }
        
        amountTextField.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(-35)
            make.height.equalTo(35)
            make.top.equalTo(noteLabel.snp.bottom).offset(35)
            make.right.equalToSuperview().offset(-20)
        }
        
        payeeLabel.snp.makeConstraints { (make) in
            make.width.equalTo(130)
            make.height.equalTo(35)
            make.top.equalTo(amountTextField.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(15)
        }
        
        payeeTextField.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(-35)
            make.height.equalTo(35)
            make.top.equalTo(amountLabel.snp.bottom).offset(35)
            make.right.equalToSuperview().offset(-20)
        }
        
        walletLabel.snp.makeConstraints { (make) in
            make.height.equalTo(25)
            make.top.equalTo(payeeTextField.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(15)
        }
        
        chooseWalletButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(walletLabel.snp.centerY)
            make.right.equalToSuperview().offset(-20)
        }
        
        categoryLabel.snp.makeConstraints { (make) in
            make.height.equalTo(25)
            make.top.equalTo(chooseWalletButton.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(15)
        }
        
        chooseCategoryButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(categoryLabel.snp.centerY)
            make.right.equalToSuperview().offset(-20)
        }
        
        transferLabel.snp.makeConstraints { (make) in
            make.height.equalTo(25)
            make.top.equalTo(chooseWalletButton.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(15)
        }
        
        transferToWallet.snp.makeConstraints { (make) in
            make.centerY.equalTo(transferLabel.snp.centerY)
            make.right.equalToSuperview().offset(-20)
        }
    }
    
    func subview() {
        view.addSubview(incomeButton)
        view.addSubview(outcomeButton)
        view.addSubview(transactionButton)
        view.addSubview(txtDatePicker)
        view.addSubview(selectDateLabel)
        view.addSubview(noteTextField)
        view.addSubview(noteLabel)
        view.addSubview(amountTextField)
        view.addSubview(amountLabel)
        view.addSubview(payeeTextField)
        view.addSubview(payeeLabel)
        view.addSubview(activityIndicator)
        view.addSubview(walletLabel)
        view.addSubview(chooseWalletButton)
        view.addSubview(categoryLabel)
        view.addSubview(chooseCategoryButton)
        view.addSubview(transferLabel)
        view.addSubview(transferToWallet)
    }
}
