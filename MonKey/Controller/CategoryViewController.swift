import UIKit
import Apollo

class CategoryViewController: UIViewController {
    let cellID = "cell"
    var categoryID = [GraphQLID]()
    var categoryName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        getCategoryID()
        addSubviews()
        layout()
    }
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.OperationColor.categoryColor.cgColor
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.register(CategoryCell.self, forCellReuseIdentifier: cellID)
        view.dataSource = self
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.separatorStyle = .none
        return view
    }()
    
    func getCategoryID() {
        LoginManager.shared.apollo.fetch(query: MeQuery()) { (result, error) in
            if let error = error {
                print("\(error)")
            }
            
            guard let result = result?.data else { return }
            guard let me = result.me else { return }
            guard let income = me.incomeCategories else { return }
            guard let expense = me.expenseCategories else { return }
            
            let incomeId = income.compactMap{$0.id}
            let expenseId = expense.compactMap{$0.id}
            
            let incomeName = income.compactMap{$0.name}
            let expenseName = expense.compactMap{$0.name}
            
            self.categoryID.append(contentsOf: incomeId)
            self.categoryID.append(contentsOf: expenseId)
            
            self.categoryName.append(contentsOf: incomeName)
            self.categoryName.append(contentsOf: expenseName)
            
            self.tableView.reloadData()
        }
    }
    
    func addSubviews() {
        view.addSubview(containerView)
    }
    
    func layout() {
        containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 5).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        containerView.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view != containerView {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension CategoryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryName.count == 0 ? 1 : categoryName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        if categoryName.count == 0 {
            cell.textLabel?.text = "Загружаю..."
        } else {
            cell.textLabel?.text = "\(categoryName[indexPath.row])"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = categoryID[indexPath.row]
        NewOperationViewController.categoryId = id
        self.dismiss(animated: true, completion: nil)
    }
}
